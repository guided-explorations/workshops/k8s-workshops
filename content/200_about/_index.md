---
title: "About This Workshop"
weight: 100
chapter: true
draft: false
description: "About this workshop."
#pre: '<i class="fa fa-film" aria-hidden="true"></i> '
---

## Extension Labs to the Ultimate GitOps Workshop.

This content contains extension labs to the Ultimate GitOps Workshop.


## Workshop Authorship
This workshop was contributed by the GitLab Alliances Solutions Architecture Team.

Initial authoring by [Darwin Sanoy](https://gitlab.com/darwinjs), as always - anyone can contribute!

